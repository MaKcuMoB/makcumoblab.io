---
layout: post
title:  "Печать через Google Cloud Print в (K)Ubuntu Linux 18.04/18.10"
date:   2018-09-05 15:32:14 -0300
categories: linux
---
![Google Cloud Printer](/media/img/cloudprinthero-800x626.jpg)

В Gnome 3 при добавлении сетевой учётной записи можно включить доступ к принтерам на GCP. Это самый простой способ, но не самый удобный в использовании. Например, можно печатать документы pdf, но напечатать документ из LibreOffice не получится - в меню печати просто не окажется вашего "облачного принтера". Кроме того, каждый раз после нажатия **Ctrl+P** нужно ждать несколько секунд, пока появятся принтеры в списке.

В KDE (как в моём случае) и вовсе нет возможности штатными средствами печатать на принтеры в GCP. К нашей радости на GutHub есть проект CupsCloudPrint, с помощью которого можно добавить "облачные" принтеры в свой CUPS.

**УСТАНОВКА**

Для начала установим несколько пакетов из репозиториев:

```
sudo apt install python-cups python-httplib2 python-six
```

Потом качаем и устанавливаем сам CupsCloudPrint:

```
wget https://niftyrepo.niftiestsoftware.com/cups-cloud-print/packages/cupscloudprint_20160502-1_all.deb
sudo dpkg -i cupscloudprint_20160502-1_all.deb
sudo apt-get -f install
```

**НАСТРОЙКА**

Теперь запускаем и настраиваем утилиту:

```
sudo /usr/share/cloudprint-cups/setupcloudprint.py
```

- Пишем "Y" чтобы добавить новый аккаунт
- Пишем свой e-mail в google
- Переходим по предложенной ссылке, копируем код и вставляем его обратно в терминал. Видим такое:

```
You currently have these accounts configured:
maks........@gmail.com
Add more accounts (Y/N)?
```

- Пишем "N", жмём Enter и на вопрос "Add all Google Cloud Print printers?" пишем "N". 
- Нам будет предложено выбрать один принтер из списка - выбираем и идём дальше. 
- Указываем префикс для имени принтера (я всегда пишу "CLOUD")
- И выходим из настройки выбирая "0"
